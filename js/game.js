'use strict'

let cardInt,
    preInt,
    streak = 0;

function $(id) {
    return document.getElementById(id);
}

function getIntForCard(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function NewCard() {
  let cardColors = ["C", "S", "D", "H"];
  cardInt = getIntForCard(1, 13);
  let colorInt = getIntForCard(0, 3);
  $("card").src = "img/cardGame/" + cardInt + cardColors[colorInt] + ".png";
  if (preInt == undefined) {
    preInt = cardInt;
  }
}

function SetGameText(text, color) {
  $("status").innerHTML = text;
  $("status").style.color = color;
  $("streak").innerHTML = "Winning-streak: " + streak;
}

function CheckIfWinner(sammenlign) {
  NewCard();
  if (sammenlign(preInt, cardInt)) {
    streak++;
    SetGameText("Du gættede rigtigt!", "green");
  } else {
    streak = 0;
    SetGameText("Du gættede forkert - prøv igen!", "red");
  }
  preInt = cardInt;
}

$("under").addEventListener("click", function() {
  CheckIfWinner((a, b) => a >= b);
})

$("over").addEventListener("click", function() {
  CheckIfWinner((a, b) => a <= b);
})

NewCard();
