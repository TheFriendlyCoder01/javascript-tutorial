'use strict'

//Generelle funtioner
function $(id) {
  return document.getElementById(id);
}

function $Q(c) {
  return document.querySelectorAll(c);
}

function $S(s, newS) {
  return document.documentElement.style.setProperty(s, newS);
}

//Globale Variabler
let allAs = $Q("#sidenav a"),
  allSettingsView = $Q(".settingView"),
  allpopUps = [{
      name: "themePickBtn",
      class: ".themePicker"
    },
    {
      name: "gameBtn",
      class: ".game"
    }
  ],
  everyContent = [".game_content", ".themePickerContent"],
  allThemes = [{
      name: "dafaultTheme",
      cta: '#039BE5',
      white: '#fff',
      blue: '#1A73E8',
      lightBlue: '#E8F0FE',
      black: '#000',
      codeEditor: '#F1F3F4',
      imgPath: ""
    },
    {
      name: "darkTheme",
      cta: '#2d2d2d',
      white: '#4d4d4d',
      blue: '#fff',
      lightBlue: '#3d3d3d',
      black: '#fff',
      codeEditor: '#9d9d9d',
      imgPath: "_light"
    },
    {
      name: "greenTheme",
      cta: '#1E7E34',
      white: '#fff',
      blue: '#1E7E34',
      lightBlue: '#E8F0FE',
      black: '#000',
      codeEditor: '#F1F3F4',
      imgPath: "_light"
    },
    {
      name: "greyTheme",
      cta: 'grey',
      white: '#fff',
      blue: '#000',
      lightBlue: 'lightgrey',
      black: '#000',
      codeEditor: '#F1F3F4',
      imgPath: "_light"
    }
  ];

//Systemet
function testIfEmail(mailTekst) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mailTekst)
}

//Finder alle Href fra sidenavigationen
function GetAllHref() {
  let allHref = [];
  allAs.forEach((item) => {
    allHref.push(item.href)
  });
  return allHref;
}

//Spawner Næste og/eller forrige knapper fra CheckPageForNavigationBtns()
function CreateNavigationBtn(text, goTo) {
  const allHref = GetAllHref();
  let btn = document.createElement("BUTTON");
  btn.innerHTML = text;
  btn.classList.add("CTA")
  btn.addEventListener("click", function() {
    window.location.href = allHref[allHref.indexOf(window.location.href) + goTo]
  });
  $Q("main")[0].appendChild(btn);
};

function CheckPageForNavigationBtns() {
  //Tjekker om vi er på først side, og spawner knap
  const preBtn = window.location.href != allAs[0] ? CreateNavigationBtn("Forrige", -1) : null;
  const nextBtn = window.location.href != allAs[allAs.length - 1] ? CreateNavigationBtn("Næste", 1) : null;
}

function SetTheme(theme) {
  $S('--CTA-blue-color', theme.cta);
  $S('--white-color', theme.white);
  $S('--blue-color', theme.blue);
  $S('--light-blue-color', theme.lightBlue);
  $S('--black-color', theme.black);
  $S('--code-editor', theme.codeEditor);
  $("settingsImg").src = "img/setting" + theme.imgPath + ".png";
  $("newLetterIMG").src = "img/newsletter" + theme.imgPath + ".png";
  $("gameBtn").src = "img/game" + theme.imgPath + ".png";
  localStorage.setItem("theme", theme.name);
  $Q(".themePicker")[0].style.display = "none";
}

function PresentForNewsLetter(text, color) {
  $("newsLetterText").innerHTML = text;
  $("newsLetterText").style.color = color;
}

//Initialize
function Initialize() {
  SetThemeFromLocaleStorage();
  CheckPageForNavigationBtns();
}

//Kliks + kald af funktioner

//Skjul popups, når de klikkes
allSettingsView.forEach((item) => {
  item.addEventListener("click", function() {
    item.style.display = "none";
  });
});

//Tilmeld nyhedsbrev - klik på knap i form
$Q("#topnav .newLetteWindow button")[0].addEventListener("click", function(e) {
  e.preventDefault();
  //Få alle input felter i formen til nyhedsbrevet
  let inputs = $Q("#topnav .newLetteWindow input");
  //testIfEmail bliver valideret med regular expressions
  if (inputs[0].value != "" && testIfEmail(inputs[1].value)) {
    PresentForNewsLetter("Tak for din tilmelding!", "green");
    inputs[0].value = "";
    inputs[1].value = "";
  } else {
    if (inputs[0].value == "") {
      PresentForNewsLetter("Indtast dit navn, for at tilmelde dig nyhedsbrevet.", "red");
    } else {
      PresentForNewsLetter("Indtast en valid emailadresse, for at tilmelde dig nyhedsbrevet.", "red");
    }
  }
})

//Vælg eget tema
allpopUps.forEach((item) => {
  $(item.name).addEventListener("click", function() {
    $("status").innerHTML = "";
    $Q(item.class)[0].style.display = "flex";
  });
});

everyContent.forEach((item) => {
  $Q(item)[0].addEventListener("click", function(e) {
    e.stopPropagation(); //Ignorere klik fra parent
  });
});

allThemes.forEach((item) => {
  $(item.name).addEventListener("click", function() {
    localStorage.setItem("theme", item.name);
    SetThemeFromLocaleStorage();
  });
});

//Sæt tema, hvis brugeren tidligere har valgt et
function SetThemeFromLocaleStorage() {
  if (localStorage.getItem("theme") !== null) {
    let i = allThemes.map(function(e) {
      return e.name;
    }).indexOf(localStorage.getItem("theme"));
    SetTheme(allThemes[i]);
  }
}

Initialize();
